/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define FILENAME "bitmap"
#define ITEMS_PER_LINE 16

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
char filename_in[10];
char filename_out[10];

/* Private function prototypes -----------------------------------------------*/
void string_to_uppercase(char* my_string);

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void) 
{
    /* .bmp input file */
    FILE *fin;
    /* .h output file */
    FILE *fout;
    unsigned char byte;
    int line_items;
    int total_items;
    int count_items;
    char filenameUppercase[10];

    printf("LCD Image Converter\n");

    sprintf(filename_in,"%s.bmp",FILENAME);
    sprintf(filename_out,"%s.h",FILENAME);

    printf("Input bitmap file: %s\n",filename_in);
    printf("Output header file: %s\n",filename_out);

    /* Try to open input/output files */
    fin = fopen(filename_in,"r");

    if (fin == NULL)
    {
      fprintf(stderr,"Error opening %s file.\n", filename_in);
      return -1;
    }

    fout = fopen(filename_out,"wt");

    if (fout == NULL)
    {
      fprintf(stderr,"Error writing %s file\n", filename_out);
      return -1;
    }

    /* Compute the size of the files */
    fseek(fin,0L,SEEK_END);
    total_items = ftell(fin);
    total_items++;
    rewind(fin);
    
    /* Create the .h file */

    fprintf(fout,"/* Define to prevent recursive inclusion -------------------------------------*/\n");

    strcpy(filenameUppercase,(char*)FILENAME);
    string_to_uppercase(filenameUppercase);

    fprintf(fout,"#ifndef %s_H\n", filenameUppercase);
    fprintf(fout,"#define %s_H\n\n", filenameUppercase);


    fprintf(fout,"#if defined ( __ICCARM__ ) /*!< IAR Compiler */\n");
    fprintf(fout,"  #pragma data_alignment=4   \n");
    fprintf(fout,"#endif\n\n");   
    
    fprintf(fout,"__ALIGN_BEGIN const  unsigned char %s[%d] __ALIGN_END =\n",
        filenameUppercase,total_items);
    fprintf(fout,"{\n");

    line_items = 0;
    count_items = 0;


    /* Print bytes */
    while(!feof(fin))
    {
        if (count_items != 0)
        {
            fprintf(fout, ",");
        }
        count_items++;
        
        if (line_items == ITEMS_PER_LINE)
        {
            fprintf(fout,"\n");
            line_items = 0;
        }

        line_items++;
        fread(&byte,1,1,fin);
        fprintf(fout,"0x%02X",(int)byte);
    }
    
  fprintf(fout,"\n};\n\n");
  fprintf(fout,"#endif /* %s_H */\n\n", filenameUppercase);
  fprintf(fout, "/***END OF FILE****/\n");

  fclose(fout);
  fclose(fin);
  
  return 0;
}

/**
  * @brief  Transforms a string to uppercase
  * @param  Pointer to the target string
  * @retval None
  */
void string_to_uppercase(char* my_string)
{
    while(*my_string++ = toupper(*my_string));
}
